package com.example.student.reditreader;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RedditListFragment extends Fragment {
    private RecyclerView recyclerView;
    private ActivityCallback activiyCallBack;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activiyCallBack = (ActivityCallback)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activiyCallBack = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View view =inflater.inflate(R.layout.fragment_reddit_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));//uses layout manager to show us how the adapter works and how it is displayed on the screem

        new RedditPostTask().execute(this);//this connection does not happen instantly. Async tasks work like this

        return view;
    }

    public void updateUserInterface(){
        RedditPostAdapter adapter = new RedditPostAdapter(activiyCallBack);
        recyclerView.setAdapter(adapter);
    }
}
