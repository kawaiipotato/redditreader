import com.example.student.reditreader.R;
import com.example.student.reditreader.RedditWebActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.FloatMath;
import android.widget.Toast;


/**
 * Created by Student on 10/22/2015.
 */
   public class Shake extends RedditWebActivity {

        private SensorManager sensorManager;
        private Sensor accelerometer;
        private float acceleration;
        private float currentAcceleration;
        private float previousAcceleration;
        MediaPlayer mediaPlayer;

        private final SensorEventListener sensorListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];

                previousAcceleration = currentAcceleration;
                currentAcceleration = FloatMath.sqrt(x * x + y * y + z * z);
                float delta = currentAcceleration - previousAcceleration;
                acceleration = acceleration * 0.9f + delta;


                if(acceleration > 15){
                    Toast toast = Toast.makeText(getApplication(), "Device has shaken", Toast.LENGTH_SHORT);
                    toast.show();
                    mediaPlayer = MediaPlayer.create(Shake.this, R.raw.hey_listen);
                    mediaPlayer.start();
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };

        @Override
        protected void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_reddit_web);

            sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

            acceleration = 0.0f;
            currentAcceleration = SensorManager.GRAVITY_EARTH;
            previousAcceleration = SensorManager.GRAVITY_EARTH;


        }

        @Override
        protected void onResume() {
            super.onResume();
            sensorManager.registerListener(sensorListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }

        @Override
        protected void onPause() {
            super.onPause();
            sensorManager.unregisterListener(sensorListener);
        }
    }
}
