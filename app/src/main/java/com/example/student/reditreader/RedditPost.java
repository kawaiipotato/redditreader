package com.example.student.reditreader;

public class RedditPost {
    //store info on reddit post here
    public String title;
    public String url;

    public RedditPost(String title, String url){
        this.title = title;
        this.url = url;
    }

}
