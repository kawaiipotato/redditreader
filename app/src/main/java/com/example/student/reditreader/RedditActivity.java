package com.example.student.reditreader;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;

public class RedditActivity extends SingleFragmentActivity implements ActivityCallback{

    @LayoutRes
    @Override
    protected int getLayoutResId() {

        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment(){

        return new RedditListFragment();
    }

    @Override
    public void onPostSelected(Uri redditPostUri) {
        if(findViewById(R.id.detail_fragment_container)==null) {
            Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);//lets ut launch different activities
            intent.setData(redditPostUri);
            startActivity(intent);

            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.hey_listen);
            mediaPlayer.start();

        }
        else{
            Fragment detailFragment = RedditWebFragment.newFragment(redditPostUri.toString());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container,detailFragment)
                    .commit();
        }
    }
}
